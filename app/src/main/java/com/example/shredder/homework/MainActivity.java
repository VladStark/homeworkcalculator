package com.example.shredder.homework;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
// import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    int countNumbers = 0;
    Button bClicker;
    EditText fNumber,sNumber;
    TextView gNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fNumber = (EditText) findViewById(R.id.first_number);
        sNumber = (EditText) findViewById(R.id.second_number);
        gNumber = (TextView) findViewById(R.id.give_number);
        bClicker = (Button) findViewById(R.id.button_clicker);
        bClicker.setOnClickListener(this); // Опредляется именно это кнопка
    }
    @Override
    public void onClick(View view) {
        String fnumber = fNumber.getText().toString();
        String snumber = sNumber.getText().toString();
        int firstNumber = Integer.valueOf(fnumber);
        int secondNumber = Integer.valueOf(snumber);
        countNumbers = firstNumber + secondNumber;
        gNumber.setText(String.valueOf(countNumbers));
    }
}